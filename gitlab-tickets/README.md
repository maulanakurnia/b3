# Gitlab Tickets

Video Course: [GitLab Project Management: How to use Issue, Labels, and Boards](https://youtu.be/J2u7OqBA_aQ)

## Screenshot

### Issues

![issues](screenshot/issue.png)

### Issues Details

![issues](screenshot/issue-details.png)

### Board

![board](screenshot/boards.png)

### Mailstone

![mailstones](screenshot/mailstones.png)

### Mailstone Detail

![mailstones](screenshot/mailstones-details.png)
