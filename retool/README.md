# Retools

Video Course:

- [Build a CRUD app in 6 minutes - Postgres, MySQL, MongoDB, Firebase, Supabase, DynamoDB, ReactJS](https://youtu.be/WZLwEwKVPyQ)
- [Building Your First Retool App: Product Walkthrough](https://www.youtube.com/watch?v=lqFgt4_BS6o&t=349s)
- [Retool Crash Course - Build an Admin Panel in 20 minutes](https://www.youtube.com/watch?v=nUf8R4wFIrE)
- [How to Integrate and Visualize Your REST API with Retool](https://www.youtube.com/watch?v=XxNpCEVHaO8)

## Screenshot
